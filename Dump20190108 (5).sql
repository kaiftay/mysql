-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: DB2
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bids` (
  `bid_id` int(11) NOT NULL AUTO_INCREMENT,
  `bid_date` date NOT NULL,
  `bid_value` float NOT NULL,
  `items_item_id` int(11) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`bid_id`),
  KEY `bids_fk0` (`items_item_id`),
  KEY `bids_fk1` (`users_user_id`),
  CONSTRAINT `bids_fk0` FOREIGN KEY (`items_item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `bids_fk1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
INSERT INTO `bids` VALUES (1,'2011-12-18',1,1,1),(3,'2013-12-18',6,3,3),(4,'2014-12-18',4,4,4),(5,'2015-12-18',5,5,5),(6,'2016-12-18',6,6,1),(8,'2018-12-18',16,8,3),(9,'2019-12-18',9,9,4),(10,'2011-12-18',1,1,1),(12,'2013-12-18',6,3,3),(13,'2014-12-18',4,4,4),(14,'2015-12-18',5,5,5),(15,'2016-12-18',6,6,1),(17,'2018-12-18',16,8,3),(18,'2019-12-18',9,9,4);
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `star_price` float NOT NULL,
  `bid_increment` float NOT NULL,
  `start_data` date NOT NULL,
  `stop_date` date NOT NULL,
  `by_it_now` binary(1) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `items_fk0` (`users_user_id`),
  CONSTRAINT `items_fk0` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'item1','some item',0.1,1,'2011-12-18','2019-12-18',_binary 'ÿ',1),(3,'item3','some item',0.3,3,'2011-12-18','2011-12-18',_binary 'ÿ',3),(4,'item4','some item',0.4,4,'2011-12-18','2011-12-18',_binary 'ÿ',4),(5,'item5','some item',0.5,5,'2011-12-18','2011-12-18',_binary 'ÿ',5),(6,'item6','some item',0.6,6,'2011-12-18','2011-12-18',_binary 'ÿ',1),(8,'item8','some item',0.8,8,'2011-12-18','2011-12-18',_binary 'ÿ',3),(9,'item9','some item',0.9,9,'2011-12-18','2011-12-18',_binary 'ÿ',4),(10,'item1','some item',0.1,1,'2011-12-18','2011-12-18',_binary 'ª',1),(11,'item1','some item',0.1,1,'2011-12-18','2011-12-18',_binary 'ª',1),(12,'item1','some item',0.1,1,'2011-12-18','2011-12-18',_binary 'ª',1),(13,'item1','some item',0.1,1,'2011-12-18','2011-12-18',_binary 'ª',1),(14,'item1','some item',0.1,1,'2011-12-18','2011-12-18',_binary 'ª',1),(15,'item1','some item',0.1,1,'2011-12-18','2011-12-18',_binary 'ª',1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(45) NOT NULL,
  `billing_address` varchar(100) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ivanov Ivan1','St. Pushina Kalatushkina1','login1','secure'),(2,'Ivanov Ivan2','St. Pushina Kalatushkina2','login2','secure'),(3,'Ivanov Ivan3','St. Pushina Kalatushkina3','login3','secure'),(4,'Ivanov Ivan4','St. Pushina Kalatushkina4','login4','secure'),(5,'Ivanov Ivan5','St. Pushina Kalatushkina5','login5','secure'),(6,'Dima Dimanov','Moscov','Login','Password'),(7,'Dima Dimanov','Moscov','Login','Password'),(8,'Dima Dimanov','Moscov','Login','Password'),(9,'Dima Dimanov','Moscov','Login','Password'),(10,'Dima Dimanov','Moscov','Login','Password'),(11,'Dima Dimanov','Moscov','Login','Password'),(12,'Dima Dimanov','Moscov','Login','Password');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'DB2'
--

--
-- Dumping routines for database 'DB2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 23:26:37
update bids set bid_value = bid_value*2 where users_user_id = 3;
delete from items where users_user_id = 2;
delete from bids where users_user_id = 2;
insert into items(title,description,star_price, bid_increment, start_data,stop_date, by_it_now,users_user_id) values ("item1","some item",0.1,1,"2011-12-18 13:17:17", "2011-12-18 16:17:17", UNHEX('AA'), 1);
insert into users (full_name, billing_address, login, password) values ("Dima Dimanov", "Moscov", "Login", "Password");
select title from users cross join items on users.user_id= items.users_user_id where user_id = 1 and curdate() < stop_date;
select title, MAX(bid_value) from bids cross join items on bids.items_item_id = items.item_id group by item_id;
select full_name, AVG(star_price) from users cross join items on users.user_id = items.users_user_id group by user_id;
select * from items where `description` LIKE '%item%';
select * from items where `title` LIKE '%item%';
select * from items where users_user_id = 1;
select * from bids where users_user_id = 1;