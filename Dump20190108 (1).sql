CREATE DATABASE  IF NOT EXISTS `DB1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `DB1`;
-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: DB1
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `marks` (
  `marks_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(45) NOT NULL,
  `mark` int(45) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`marks_id`),
  KEY `marks_fk0` (`student_id`),
  CONSTRAINT `marks_fk0` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks`
--

LOCK TABLES `marks` WRITE;
/*!40000 ALTER TABLE `marks` DISABLE KEYS */;
INSERT INTO `marks` (`marks_id`, `subject`, `mark`, `student_id`) VALUES (41,'java',4,1),(42,'java',3,1),(43,'sql',5,1),(44,'sql',3,1),(45,'java',2,1),(46,'java',3,1),(47,'sql',3,1),(48,'sql',4,1),(49,'java',2,2),(50,'java',5,2),(51,'sql',3,2),(52,'sql',4,2),(53,'java',4,3),(54,'java',3,3),(55,'sql',5,3),(56,'sql',3,3),(57,'java',5,4),(58,'java',2,4),(59,'sql',4,4),(60,'sql',3,4),(61,'java',4,5),(62,'java',3,5),(63,'sql',5,5),(64,'sql',3,5),(65,'java',1,6),(66,'java',2,6),(67,'sql',4,6),(68,'sql',3,6),(69,'java',4,7),(70,'java',3,7),(71,'sql',5,7),(72,'sql',3,7),(73,'java',5,8),(74,'java',2,8),(75,'sql',1,8),(76,'sql',4,8),(77,'java',4,9),(78,'java',3,9),(79,'sql',5,9),(80,'sql',3,9),(81,'java',4,11),(82,'java',3,11),(83,'sql',5,11),(84,'sql',3,11),(85,'java',2,1),(86,'java',3,1),(87,'sql',3,1),(88,'sql',4,1),(89,'java',2,2),(90,'java',5,2),(91,'sql',3,2),(92,'sql',4,2),(93,'java',4,3),(94,'java',3,3),(95,'sql',5,3),(96,'sql',3,3),(97,'java',5,4),(98,'java',2,4),(99,'sql',4,4),(100,'sql',3,4),(101,'java',4,5),(102,'java',3,5),(103,'sql',5,5),(104,'sql',3,5),(105,'java',1,6),(106,'java',2,6),(107,'sql',4,6),(108,'sql',3,6),(109,'java',4,7),(110,'java',3,7),(111,'sql',5,7),(112,'sql',3,7),(113,'java',5,8),(114,'java',2,8),(115,'sql',1,8),(116,'sql',4,8),(117,'java',4,9),(118,'java',3,9),(119,'sql',5,9),(120,'sql',3,9);
/*!40000 ALTER TABLE `marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `group` varchar(45) NOT NULL,
  `sex` varchar(1) NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`student_id`, `name`, `group`, `sex`) VALUES (1,'zxc','b','m'),(2,'fre','a','f'),(3,'Петров','a','m'),(4,'xzcvb','a','f'),(5,'rty','b','f'),(6,'wergh','b','f'),(7,'likmy','b','f'),(8,'iubtr','b','f'),(9,'oluimynut','b','m'),(11,'asd','a','f');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'DB1'
--

--
-- Dumping routines for database 'DB1'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 22:24:09
select * from student;
select * from student order by name;
select name, mark from student cross JOIN marks ON student.student_id = marks.student_id where subject = "java";
select name ,AVG (mark) from student cross join marks on student.student_id = marks.student_id where name="Петров";
select subject,AVG (mark) from marks group by subject ;
select `group`,AVG(mark) from student  cross join marks on student.student_id = marks.student_id where subject = "java" group by `group`;
select name, COUNT(*) from student cross join marks on student.student_id = marks.student_id group by name;